class Comment < ActiveRecord::Base
	# set up the many-to-one relationship with posts.
	belongs_to :post

	# set up validation options
	validates_presence_of :post_id
	validates_presence_of :body
end
