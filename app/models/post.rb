class Post < ActiveRecord::Base
	# set up the many-to-one relationship with comments. also, destroy
	# any comments associated with a destroyed post
	has_many :comments, dependent: :destroy 

	# set up validation options
	validates_presence_of :title
	validates_presence_of :body	
end
